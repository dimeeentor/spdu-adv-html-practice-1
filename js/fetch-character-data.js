import {BASE_URL} from './constants'
import {queryBuilder} from './utils/query-builder';

export const fetchCharacterData = id => (
    fetch(queryBuilder(`${BASE_URL}/characters/${id}`)).then(response => (
        response.json()
    ))
);
