export const getSinglePageTemplate = ({id, name, thumbnail, description}) => (
    `<div class="card">
        <p>${name}</p>
        <img src="${thumbnail}" alt="${name}" class="card-image">
        <p>${description}</p>
    </div>`
);
